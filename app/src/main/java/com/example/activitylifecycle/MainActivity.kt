package com.example.activitylifecycle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Toast.makeText(getApplicationContext(),"App state: OnCreate",Toast.LENGTH_LONG).show();
    }
    override fun onStart() {
        super.onStart()
        print("***App state: OnStart***n")
        Toast.makeText(getApplicationContext(),"App state: OnStart",Toast.LENGTH_LONG).show();
    }
    override fun onResume() {
        super.onResume()
        print("***App state: OnResume***n")
        Toast.makeText(getApplicationContext(),"App state: OnResume",Toast.LENGTH_LONG).show();
    }
    override fun onStop() {
        super.onStop()
        print("***App state: OnStop***n")
        Toast.makeText(getApplicationContext(),"App state: OnStop",Toast.LENGTH_LONG).show();
    }
    override fun onPause() {
        super.onPause()
        print("***App state: OnPause***n")
        Toast.makeText(getApplicationContext(),"App state: OnPause", Toast.LENGTH_LONG).show();
    }
    override fun onRestart() {
        super.onRestart()
        print("***App state: OnReStart***n")
        Toast.makeText(getApplicationContext(),"App state: OnRestart",Toast.LENGTH_LONG).show();
    }

    override fun onDestroy() {
        super.onDestroy()
        print("***App state: OnDestroy***n")
        Toast.makeText(getApplicationContext(),"App state: OnDestroy",Toast.LENGTH_LONG).show();
    }
}